<?php


namespace PBH\GoogleFeed\Cron;
use PBH\GoogleFeed\Model\XmlWriter;

class DeleteOldFeeds
{
    public $xmlWriter;

    public function __construct(XmlWriter $xmlWriter)
    {
        $this->xmlWriter = $xmlWriter;
    }

    public function execute()
    {
        $this->xmlWriter->deleteOldFiles();
    }
}