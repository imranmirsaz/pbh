<?php

namespace PBH\GoogleFeed\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Request\Http;
use PBH\GoogleFeed\Model\ProductsFeed;

/**
 * Class Index
 * @package PBH\GoogleFeed\Controller\Index
 */
class Index Extends Action
{
    /**
     * @var Http
     */
    public $request;
    /**
     * @var ProductsFeed
     */
    public $productsFeed;

    /**
     * Index constructor.
     * @param Context $context
     * @param Http $request
     * @param ProductsFeed $productsFeed
     */
    public function __construct(
        Context $context,
        Http $request,
        ProductsFeed $productsFeed
    ) {
        parent::__construct($context);
        $this->request = $request;
        $this->productsFeed = $productsFeed;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        set_time_limit(0);
        ini_set("memory_limit", -1);

        $cache = $this->request->getParam('cache');
        // generate a new Feed File
        if (isset($cache) and $cache == 'clear') {
            $this->productsFeed->generateFeed();
        }
        //Load Feed to the Browser
        $this->productsFeed->loadFeed();
    }
}