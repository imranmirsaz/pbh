<?php

namespace PBH\GoogleFeed\Model;

use Magento\Framework\DomDocument\DomDocumentFactory;


use PBH\GoogleFeed\Helper\DirectoryManager;
/**
 * Class XmlWriter
 * @package PBH\GoogleFeed\Model
 */
class XmlWriter
{

    /**
     * @var DomDocumentFactory
     */
    public $domFactory;
    /**
     * @var DirectoryList
     */
    public $directoryManager;

    /**
     * XmlWriter constructor.
     * @param DomDocumentFactory $domFactory
     * @param DirectoryManager $directoryManager
     */
    public function __construct(
        DomDocumentFactory $domFactory,
        DirectoryManager $directoryManager
    ) {

        $this->domFactory = $domFactory;
        $this->directoryManager = $directoryManager;
    }

    /**
     * @param $xml
     * @return mixed
     */
    public function createFeedNode($xml)
    {
        //  Create Root Node 'feed Node'
        $feedNode = $xml->createElement("feed");
        //  Add Atom XML Namespace Attribute to feed node
        $domAttribute = $xml->createAttribute('xmlns');
        $domAttribute->value = 'http://www.w3.org/2005/Atom';
        $feedNode->appendChild($domAttribute);
        //  Add Google Base XML Namespace Attribute to feed node
        $domAttribute = $xml->createAttribute('xmlns:g');
        $domAttribute->value = 'http://base.google.com/ns/1.0';

        $feedNode->appendChild($domAttribute);

        return $feedNode;
    }

    /**
     * @param $xml
     * @param $product
     * @return mixed
     */
    public function createEntryNode($xml, $product)
    {
        //entry
        $entryNode = $xml->createElement("entry");

        //Link Added
        $idNode = $xml->createElement("g:id");
        $cdataId = $xml->createCDATASection($product['id']);
        $idNode->appendChild($cdataId);
        $entryNode->appendChild($idNode);


        //Link Added
        $linkNode = $xml->createElement("g:link");
        $cdataLink = $xml->createCDATASection($product['link']);
        $linkNode->appendChild($cdataLink);
        $entryNode->appendChild($linkNode);

        //Shipping Added
        $shippingNode = $xml->createElement("g:shipping");

        //Shipping Country
        $shippingCountryNode = $xml->createElement("g:country", "GB");
        $shippingNode->appendChild($shippingCountryNode);

        $shippingPriceNode = $xml->createElement('g:price', $product['shipping_price']);
        $shippingNode->appendChild($shippingPriceNode);

        $shippingServiceNode = $xml->createElement('g:service', "UPS");
        $shippingNode->appendChild($shippingServiceNode);

        $entryNode->appendChild($shippingNode);

        //Description Added
        $descriptionNode = $xml->createElement("g:description");
        $cdataDescription = $xml->createCDATASection($product['description']);
        $descriptionNode->appendChild($cdataDescription);
        $entryNode->appendChild($descriptionNode);


        //title Added
        $titleNode = $xml->createElement("g:title");
        $cdataTitle = $xml->createCDATASection($product['title']);
        $titleNode->appendChild($cdataTitle);
        $entryNode->appendChild($titleNode);

        //Currency Added
        $currencyNode = $xml->createElement("g:currency", "GBP");
        $entryNode->appendChild($currencyNode);

        //Condition Added
        $conditionNode = $xml->createElement("g:condition", "NEW");
        $entryNode->appendChild($conditionNode);

        //Availability Added
        $availabilityNode = $xml->createElement("g:availability", "IN STOCK");
        $entryNode->appendChild($availabilityNode);

        //Id Added
        $idNode = $xml->createElement("g:id", $product['sales_code']);
        $entryNode->appendChild($availabilityNode);



        //price Added
        $priceNode = $xml->createElement("g:price");
        $cdataPrice = $xml->createCDATASection($product['price']);
        $priceNode->appendChild($cdataPrice);
        $entryNode->appendChild($priceNode);

        //Image Added
        $imageNode = $xml->createElement("g:image_link");
        $cdataImage = $xml->createCDATASection($product['image_link']);
        $imageNode->appendChild($cdataImage);
        $entryNode->appendChild($imageNode);

        if($product['google_product_type'] !=''){
            //Google Product Type Added
            $typeNode = $xml->createElement("g:product_type");
            $cdataType = $xml->createCDATASection($product['google_product_type']);
            $typeNode->appendChild($cdataType);
            $entryNode->appendChild($typeNode);

        }

        if($product['google_product_category'] != ''){

            //Google Product Category Added
            $categoryNode = $xml->createElement("g:google_product_category");
            $cdataCategory = $xml->createCDATASection($product['google_product_category']);
            $categoryNode->appendChild($cdataCategory);
            $entryNode->appendChild($categoryNode);

        }


        if($product['size'] != ''){
            //tax_id Added
            $sizeNode = $xml->createElement("g:size");
            $cdataSize = $xml->createCDATASection($product['size']);
            $sizeNode->appendChild($cdataSize);
            $entryNode->appendChild($sizeNode);
        }

        if($product['material'] != ''){
            //tax_id Added
            $materialNode = $xml->createElement("g:material");
            $cdataMaterial = $xml->createCDATASection($product['material']);
            $materialNode->appendChild($cdataMaterial);
            $entryNode->appendChild($materialNode);
        }

        if($product['color'] != ''){
            //tax_id Added
            $colorNode = $xml->createElement("g:color");
            $cdataColor = $xml->createCDATASection($product['color']);
            $colorNode->appendChild($cdataColor);
            $entryNode->appendChild($colorNode);
        }


        if($product['meta_title'] != ''){
            $metaTitleNode = $xml->createElement("seo_meta_title");
            $cdataMetaTitle = $xml->createCDATASection($product['meta_title']);
            $metaTitleNode->appendChild($cdataMetaTitle);
            $entryNode->appendChild($metaTitleNode);
        }

        if($product['meta_keywords'] != ''){
            $metaKeywordsNode = $xml->createElement("seo_meta_keywords");
            $cdataMetaKeywords = $xml->createCDATASection($product['meta_keywords']);
            $metaKeywordsNode->appendChild($cdataMetaKeywords);
            $entryNode->appendChild($metaKeywordsNode);
        }


        if($product['gender'] == ''){
            $product['gender'] = 'unisex';
        }

        $genderNode = $xml->createElement("g:gender");
        $cdataGender = $xml->createCDATASection($product['gender']);
        $genderNode->appendChild($cdataGender);
        $entryNode->appendChild($genderNode);


        if($product['age_group'] == ''){
            $product['age_group'] = 'adult';
        }

        $ageGroupNode = $xml->createElement("g:age_group");
        $cdataAgeGroup = $xml->createCDATASection($product['age_group']);
        $ageGroupNode->appendChild($cdataAgeGroup);
        $entryNode->appendChild($ageGroupNode);
       
        // STU: the value could be false,no or 0, if false, no or 0 the gtin and mpn should not bet set
        if( strtolower($product['identifier_exists']) == 'false' 
           || strtolower($product['identifier_exists']) == 'no' 
           || $product['identifier_exists'] == '0' )
        {     
            // do not include gtin or mpn
            // only include identifier_exists if we need to change from the default value which is YES/TRUE
            $identifierNode = $xml->createElement("g:identifier_exists", $product['identifier_exists']);
            $entryNode->appendChild($identifierNode);
        }
        else
        {     
            
            // identifier_exists must be true but we don't need to add the identifier_exists node as it defaults to true anyway
            
            // mpn
            $mpnNode = $xml->createElement("g:mpn");
            $mpnData  = (empty($product['mpn']) ? $product['sales_code'] : $product['mpn']);
            $cdataMpn = $xml->createCDATASection($mpnData);
            $mpnNode->appendChild($cdataMpn);
            $entryNode->appendChild($mpnNode);

            // gtin
            $gtinNode = $xml->createElement("g:gtin");
            $cdataGtin = $xml->createCDATASection($product['gtin']);
            $gtinNode->appendChild($cdataGtin);
            $entryNode->appendChild($gtinNode);
               
            // Brand
            $brandNode = $xml->createElement("g:brand");
            $cdataBrand = $xml->createCDATASection($product['brand']);
            $brandNode->appendChild($cdataBrand);
            $entryNode->appendChild($brandNode);
            
    
        }

        return $entryNode;
    }

    /**
     * @param $xml
     * @param $feedNode
     */
    public function createAndOutputXml($xml, $feedNode)
    {
        $root = $this->directoryManager->getDirectoryPath();
        $time = gmdate('dmY_his');
        $path = $root . "products_" . $time . '.xml';
        $xml->appendChild($feedNode);
        $xml->formatOutput = TRUE;
        $xml->save($path);
    }

    /**
     * @return bool|string
     */
    public function outputXml()
    {
        $path = $this->getLastFilePath();
        if(!$path) return false;
        $xml = $this->generateXmlHeader();
        $xml->load($path);
        ob_clean();
        header('Content-Type: text/xml;');
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        $xml->formatOutput = TRUE;
        $feed = $xml->saveXML();
        echo $feed;
        exit;
        // We cant let magento handle the output as it will cause headers already sent issues since magento 2.3.3
        //return $feed;
    }

    /**
     * @return bool|int|string|null
     */
    public function getLastFilePath()
    {
        $root = $this->directoryManager->getDirectoryPath();
        $path = $root . "products_*.xml";

        $files = glob($path);

        $files = array_combine($files, array_map('filectime', $files));
        arsort($files);
        $path = key($files); // the filename
        if ($path != '') return $path;
        return false;

    }

    /**
     * @return \DOMDocument
     */
    public function generateXmlHeader()
    {
        //  Create new instance of a XML Object
        $xml = $this->domFactory->create("1.0", "ISO-8859-15");
        $xml->recover = TRUE;
        return $xml;
    }

    /**
     * @param $path
     * @return bool
     */
    public function isFileExpired($path)
    {
        if (filemtime($path) < strtotime('-7 days')) {
            return true;
        }

        return false;
    }

    public function deleteOldFiles()
    {
        $root = $this->directoryManager->getDirectoryPath();
        $path = $root . "products_*.xml";

        $files = glob($path);
        foreach($files as $file){

           if($this->isFileExpired($file)){
              unlink($file);
          }
        }
    }


}
