<?php

namespace PBH\GoogleFeed\Model;

use Psr\Log\LoggerInterface;

/**
 * Class ProductsFeed
 * @package PBH\GoogleFeed\Model
 */
class ProductsFeed
{

    /**
     * @var ProductsCollection
     */
    public $productsCollection;
    /**
     * @var XmlWriter
     */
    public $xmlWriter;
    /**
     * @var LoggerInterface
     */
    private $logger;

    private static $logEnabled = 0;

    /**
     * ProductsFeed constructor.
     * @param ProductsCollection $productsCollection
     * @param XmlWriter $xmlWriter
     */
    public function __construct(
        ProductsCollection $productsCollection,
        XmlWriter $xmlWriter,
        LoggerInterface $logger
    ) {
        $this->productsCollection = $productsCollection;
        $this->xmlWriter = $xmlWriter;
        $this->logger = $logger;
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function generateFeed($limit = null, $storeId = null)
    {
        $products = $this->productsCollection->getProducts($limit,$storeId);
     //   $products =  $this->productsCollection->getSingleProduct(472);
        if (count($products) > 0) {
            $xml = $this->xmlWriter->generateXmlHeader();
            $feedNode = $this->xmlWriter->createFeedNode($xml);

            foreach ($products as $product) {
                $this->addLog(sprintf('Product feed processes: %s', $product->getSku()));
                $processedProduct = $this->productsCollection->processProduct($product);
                if($processedProduct){
                    $entryNode = $this->xmlWriter->createEntryNode($xml, $processedProduct);
                    $feedNode->appendChild($entryNode);
                    $this->addLog(sprintf('Product feed add entry in the feed: %s', $product->getSku()));
                }

            }

            $this->xmlWriter->createAndOutputXml($xml, $feedNode);
        }
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function LoadFeed()
    {
        $feed = $this->xmlWriter->outputXml();

        if($feed == false){ // if no feed is generated before, generate a new feed now.
           $this->generateFeed();
            $feed = $this->xmlWriter->outputXml();
        }

        if($feed){
         print $feed;
        }
    }

    private function addLog($message): void
    {
        if (self::$logEnabled) {
            $this->logger->info($message);
        }
    }
}