<?php

namespace PBH\GoogleFeed\Model;

use Magento\Catalog\Model\Product\Visibility;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use PBH\GoogleFeed\Model\ProductCollection\ProductDataProvider\ProductData;
use PBH\GoogleFeed\Model\ProductCollection\ProductReaderWithParentFallback;
use PBH\GoogleFeed\Model\ProductCollection\ShippingReader;

/**
 * Class ProductsCollection
 * @package PBH\GoogleFeed\Model
 */
class ProductsCollection
{
    private $selectValueAttributes = [
        'identifier_exists'=>'google_identifier_exists',
        'color'=>'color',
        'brand'=>'product_brand',
        'gender'=>'feed_gender',
        'size'=>'feed_size',
        'material'=>'feed_material'
    ];
    /**
     * @var StoreManagerInterface
     */
    public $storeManager;

    /**
     * @var ProductCollection\ShippingReader
     */
    private $shippingReader;
    /**
     * @var ProductCollection\ProductReaderWithParentFallback
     */
    private $productReader;

    /**
     * @var Review\Summary
     */
    private $summary;
    private $RatingsAndReviews;

    /**
     * @var RatingsAndReviews
     */
    private $ratingsAndReviews;
    /**
     * @var ProductData
     */
    private $productData;
    private $DataAccessType;


    public function __construct(
        StoreManagerInterface $storeManager,
        ProductReaderWithParentFallback $productReader,
        ShippingReader $shippingReader,
        RatingsAndReviews $ratingsAndReviews,
        ProductData $productData

    ) {
        $this->storeManager = $storeManager;
        $this->shippingReader = $shippingReader;
        $this->productReader = $productReader;
        $this->ratingsAndReviews = $ratingsAndReviews;
        $this->productData = $productData;
    }


    public function getProducts($limit = null, $store_id = null,$accessType = null)
    {
        $this->setDataAccessType($accessType);
        return $this->productData->getProducts($limit,$store_id);

    }


    public function getSingleProduct($productId)
    {
        return $this->productData->getSingleProduct($productId);
    }

    /**
     * @param $product
     * @param null $dataType
     * @return array|bool
     * @throws NoSuchEntityException
     */
    public function processProduct($product)
    {
        $result = [];
        // product, parent product, linked products are not visible so we are not adding this product into feed
        if (!$this->isProductEligibleToBeInTheFeed($product)) return false;

        $selectAttributes = $this->productData->getSelectAttributesValues($product, $this->selectValueAttributes);
        $result = array_merge($result, $selectAttributes);

        $result['link'] = $this->productReader->getProductUrlFallbackOnParentIfNotVisible($product, $this->getDataAccessType());
        $result['id'] = $product->getSalesCode();
        $result['title'] = $product->getName();
        $result['description'] = $this->productReader->getProductDescriptionFallbackOnParentIfNotVisible($product);
        $result['sales_code'] = $product->getSalesCode();
        $result['image_link'] = $this->productReader->getProductImageFallbackOnParentIfNotAvailable($product, $this->getDataAccessType());
        $result['price'] = $this->shippingReader->getProductPrice($product) . ' ' . $this->getCurrencyCode();
        $result['google_product_type'] = $product->getGoogleProductType();
        $result['google_product_category'] = $product->getGoogleProductCategory();

        // STU: the value could be false,no or 0, if false, no or 0 the gtin and mpn should not bet set
        if (strtolower($result['identifier_exists']) == 'false'
            || strtolower($result['identifier_exists']) == 'no'
            || $result['identifier_exists'] == '0') {
            $result['gtin'] = '';
            $result['mpn'] = '';
            $result['brand'] = '';
        } else {
            $result['gtin'] = $product->getUpc();
            $result['mpn'] = $product->getProductMpn();
            //$result['brand'] is set using the array_merge function above so no need to set it here.
        }


        $result['shipping_price']= $this->getShippingPrice($product);
        $result['meta_title'] = $product->getMetaTitle();
        $result['meta_keywords'] =  $product->getMetaKeywords();
        $result['age_group'] = $product->getAgeGroup();

        //Attributes for ElasticSearch
        $result['product_flash_id'] = $this->productReader->getProductAttributeFallbackOnParentIfNotAvailable($product,'product_flash_id');
        $result['no_of_reviews'] = $this->ratingsAndReviews->getProductReviews($product->getId());
        $result['ratings'] = $this->ratingsAndReviews->getRatingSummary($product);
        $result['category_name'] = $this->productReader->getProductCategoryNameFallbackOnParentIfNotAvailable($product);
        $result['parent_name'] = $this->productReader->getParentProductAttribute($product,'name');

        return $result;
    }


    /**
     * @return mixed
     * @throws NoSuchEntityException
     */
    private function getCurrencyCode()
    {
        return $this->storeManager->getStore()->getCurrentCurrency()->getCode();
    }



    /**
     * @param $product
     * @return string
     * @throws NoSuchEntityException
     */
    private function getShippingPrice($product): string
    {
        return $this->shippingReader->getShippingPrice($product) . ' ' . $this->getCurrencyCode();
    }

    /**
     * @param $product
     * @return bool
     */
    private function isProductEligibleToBeInTheFeed($product): bool
    {

        if($this->getDataAccessType() == '' ){
            if($this->isClientShopProduct($product)){
                return false;
            }
        }

        $productVisibility = $this->productReader->getProductVisibilityFallbackOnParentIfNotVisible($product);

        return $productVisibility != Visibility::VISIBILITY_NOT_VISIBLE;
    }

    private function setDataAccessType($accessType)
    {
        $this->DataAccessType =  $accessType;
    }

    private function getDataAccessType()
    {
        return $this->DataAccessType;
    }

    private function isClientShopProduct($product){

        if (
                $product->getClientShopProduct() and
                (
                    $product->getClientShopProduct() == 1
                    or $product->getClientShopProduct() == true
                    or strtolower($product->getClientShopProduct()) == 'yes'
                )
            ){
            return true;
        }
    return false;
    }

}
