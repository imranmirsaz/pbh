<?php


namespace PBH\GoogleFeed\Model;

use Magento\Review\Model\Review;
use Magento\Review\Model\ReviewFactory;
use Magento\Store\Model\StoreManagerInterface;

class RatingsAndReviews
{
    /**
     * @var ReviewFactory
     */
    private $reviewFactory;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var Review
     */
    private $review;
    /**
     * @var int
     */
    private $storeId;


    /**
     * RatingsAndReviews constructor.
     * @param Review $review
     * @param ReviewFactory $reviewFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(Review $review, ReviewFactory $reviewFactory, StoreManagerInterface $storeManager)
    {

        $this->reviewFactory = $reviewFactory;
        $this->storeManager = $storeManager;
        $this->review = $review;
        $this->setStoreId();
    }


    public function getRatingSummary($product)
    {
        $this->reviewFactory->create()->getEntitySummary($product, $this->getStoreId());
        $ratingSummary = $product->getRatingSummary()->getRatingSummary();

        return $this->formatRating($ratingSummary);
    }

    private function formatRating($rating){
        return round(5 * $rating / 100);
    }


    public function getProductReviews($getId, bool $verifiedOnly = true)
    {

        return $this->review->getTotalReviews($getId,
            $verifiedOnly,$this->getStoreId()
           );
    }

    private function setStoreId()
    {
        $this->storeId =  $this->storeManager->getStore()->getId();
    }

    private function getStoreId()
    {
        return $this->storeId;
    }
}