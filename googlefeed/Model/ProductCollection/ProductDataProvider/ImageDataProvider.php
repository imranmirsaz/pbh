<?php


namespace PBH\GoogleFeed\Model\ProductCollection\ProductDataProvider;


use Magento\Catalog\Block\Product\ImageBuilder;
use Magento\Catalog\Helper\ImageFactory;
use Magento\Catalog\Helper\Image;
use Magento\Store\Model\StoreManagerInterface;

class ImageDataProvider
{

    /**
     *
     */
    CONST IMAGE_PATH = 'media/catalog/product';
    /**
     * @var ImageFactory
     */
    private $helperImageFactory;
    /**
     * @var Image
     */
    private $imageHelper;
    /**
     * @var ImageBuilder
     */
    private $imageBuilder;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    public function __construct(ImageFactory $helperImageFactory,
                                Image $imageHelper,ImageBuilder $imageBuilder,
                                StoreManagerInterface $storeManager
    )
    {

        $this->helperImageFactory = $helperImageFactory;
        $this->imageHelper = $imageHelper;
        $this->imageBuilder = $imageBuilder;
        $this->storeManager = $storeManager;
    }

    public function getBaseImage($product)
    {
        return $this->imageBuilder->setProduct($product)->setImageId('product_base_image')->create()->getImageUrl();
    }

    public function getPlaceholderImage(){
        return $this->imageHelper->getDefaultPlaceholderUrl('image');
    }

    public function attachImageBaseUrl($getImage)
    {
        if(!$getImage) return;
        return  $this->getImageBaseUrl().$getImage;
    }

    private function getImageBaseUrl(){
        return  $this->storeManager->getStore()->getBaseUrl() . self::IMAGE_PATH;
    }

    public function getProductImage($product)
    {
        return str_replace('no_selection','',$product->getImage());
    }



}