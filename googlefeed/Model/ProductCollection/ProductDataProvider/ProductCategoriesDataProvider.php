<?php


namespace PBH\GoogleFeed\Model\ProductCollection\ProductDataProvider;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;

class ProductCategoriesDataProvider
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * ProductCategoriesDataProvider constructor.
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    public function getCategoryNamesByIds($categoryIds){

    if(empty($categoryIds)) return;

    $categories = $this->collectionFactory->create()
        ->addFieldToFilter('entity_id',['in' => $categoryIds])
        ->addAttributeToSelect('name');

    return $this->convertArrayToPipeSeparatedString($categories);



}

    private function convertArrayToPipeSeparatedString($categories)
    {
        if(empty($categories)) return;
        $categoryNames = '';
        foreach ($categories as $category){
            $categoryNames  .= $category->getName(). ' | ';
        }

        return rtrim($categoryNames,' | ');
    }
}