<?php


namespace PBH\GoogleFeed\Model\ProductCollection\ProductDataProvider;

use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\State;
use PBH\GoogleFeed\Model\ProductCollection\ConfigSetup\ThemeSetup;


class ProductData
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var StoreManager
     */
    private $storeManager;
    /**
     * @var State
     */
    private $state;
    /**
     * @var ThemeSetup
     */
    private $themeSetup;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * ProductData constructor.
     * @param ProductRepositoryInterface $productRepository
     * @param StoreManagerInterface $storeManager
     * @param State $state
     * @param ThemeSetup $themeSetup
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(ProductRepositoryInterface $productRepository,
                                StoreManagerInterface $storeManager,
                                State $state,
                                ThemeSetup $themeSetup,
                                SearchCriteriaBuilder $searchCriteriaBuilder
)
    {
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
        $this->state = $state;
        $this->themeSetup = $themeSetup;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    public function getProducts($limit = null, $store_id = null)
    {
        $storeId = $this->storeManager->getStore()->getId();

        if(php_sapi_name() === 'cli' and $store_id != null){
            $storeId = $store_id;
            $this->state->setAreaCode('frontend');
            $this->storeManager->setCurrentStore($storeId);
            $this->themeSetup->setDesignTheme();
        }

        $this->searchCriteriaBuilder->addFilter('status', Status::STATUS_ENABLED);
        $this->searchCriteriaBuilder->addFilter('type_id', Type::TYPE_SIMPLE);
        $this->searchCriteriaBuilder->addFilter('store_id', $storeId);

        if ($limit !== null) {
            $this->searchCriteriaBuilder->setPageSize($limit);
        }

        $searchCriteria = $this->searchCriteriaBuilder->create();
        $products = $this->productRepository->getList($searchCriteria);

        return $products->getItems();

    }

    public function getSingleProduct($productId)
    {
        $products[] = $this->productRepository->getById($productId);
        return $products;
    }


    public function getSelectAttributesValues($product, $attributeArray){
        $selectAttributeValues = [];

        foreach ($attributeArray as $key => $attribute_id) {
            $selectAttributeValues[$key] = $product->getResource()->getAttribute($attribute_id)->getFrontend()->getValue($product);
        }
        return $selectAttributeValues;
    }
}