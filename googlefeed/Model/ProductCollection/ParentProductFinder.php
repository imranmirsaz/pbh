<?php


namespace PBH\GoogleFeed\Model\ProductCollection;


use Magento\Bundle\Model\Product\Type as BundleProduct;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\Link as ProductLink;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\GroupedProduct\Model\Product\Type\Grouped as GroupedProduct;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use PBH\GoogleFeed\Model\ProductCollection\ProductDataProvider\ImageDataProvider;
use PBH\MultiplePriceGrids\Model\Product\Link as CustomLink;

class ParentProductFinder
{
    /**
     * @var Configurable
     */
    private $catalogProductTypeConfigurable;
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;
    /**
     * @var BundleProduct
     */
    private $bundleProduct;
    /**
     * @var GroupedProduct
     */
    private $groupedProduct;
    /**
     * @var ProductCollectionFactory
     */
    private $productCollectionFactory;
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Product[]
     */
    protected $parentProducts = [];
    /**
     * @var CustomLink
     */
    private $customLinks;
    /**
     * @var ProductLink
     */
    private $link;
    /**
     * @var ImageDataProvider
     */
    private $imageDataProvider;
    /**
     * @var ProductFactory
     */
    private $productFactory;

    public function __construct(
        Configurable $catalogProductTypeConfigurable,
        ResourceConnection $resourceConnection,
        BundleProduct $bundleProduct,
        GroupedProduct $grouped,
        ProductCollectionFactory $collectionFactory,
        ProductRepositoryInterface $productRepository,
        StoreManagerInterface $storeManager,
        CustomLink $customLinks,
        ProductLink $link,
    ImageDataProvider $imageDataProvider,
        ProductFactory $productFactory
    ) {
        $this->catalogProductTypeConfigurable = $catalogProductTypeConfigurable;
        $this->resourceConnection = $resourceConnection;
        $this->bundleProduct = $bundleProduct;
        $this->groupedProduct = $grouped;
        $this->productCollectionFactory = $collectionFactory;
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
        $this->customLinks = $customLinks;
        $this->link = $link;
        $this->imageDataProvider = $imageDataProvider;
        $this->productFactory = $productFactory;
    }

    /**
     * @param $product
     * @return array|bool
     * @throws NoSuchEntityException
     * select e.*, parent.sku from catalog_product_link as link
    inner join catalog_product_entity as e on e.row_id=link.linked_product_id
    inner join catalog_product_entity as parent on parent.row_id=link.product_id
    where link.link_type_id=3
    and e.row_id<>e.entity_id
    and parent.type_id=‘grouped’
    limit 4;
     */

    public function getParentOrLinkedProduct($product)
    {
        if (!$this->getCacheKey($product->getId())) {
            $result = [];

            $parentsByChild = $this->getConfigurableProductFromChildId($product);
            $parentType = 'configurable_';

            if (count($parentsByChild) < 1) {
                $parentsByChild = $this->getBundleProductFromChildId($product);
                $parentType = 'bundled_';
            }

            if (count($parentsByChild) < 1) {
                $parentsByChild = $this->getGroupProductFromChildId($product);
                $parentType = 'grouped_';
            }

            $parentProduct = $this->getEnableAndVisibleNonCustomisedProducts($product, $parentsByChild, $parentType);

            if ($parentProduct) $result = [$parentProduct, $parentType . 'parent'];

            $linkParent = $this->getLinkedParent($product->getId());
            if ($linkParent) $result = [$linkParent, 'price_grid'];

            if (empty($result) and $parentsByChild) {// iterate through grandParents
                foreach ($parentsByChild as $parentByChild) {
                    $_parentProduct = $this->productRepository->getById($parentByChild);
                    if ($_parentProduct) {
                        $result = $this->getParentOrLinkedProduct($_parentProduct);
                    }
                }
            }
            $this->cacheParentProduct($product->getId(), $result);
        }
        return $this->getCacheKey($product->getId());
    }

    /**
     * @param $entityId
     * @return string
     */
    private function getRowId($entityId)
    {
        $select = $this->getConnection()->select()
            ->from($this->getConnection()
                ->getTableName('catalog_product_entity'), 'row_id');
        $select->where('entity_id in (?)', $entityId);
        return $this->getConnection()->fetchOne($select);
    }

    /**
     * @return AdapterInterface
     */
    private function getConnection()
    {
        return $this->resourceConnection->getConnection();
    }

    /**
     * @param $rowId
     * @return string
     */
    private function getEntityId($rowId)
    {
        $select = $this->getConnection()->select()
            ->from($this->getConnection()
                ->getTableName('catalog_product_entity'), 'entity_id');
        $select->where('row_id in (?)', $rowId);
        return $this->getConnection()->fetchOne($select);
    }

    /**
     * @param $product
     * @param $parentIds
     * @param $parentType
     * @return array|bool
     * @throws NoSuchEntityException
     */
    private function getEnableAndVisibleNonCustomisedProducts($product, $parentIds, $parentType)
    {
        $parentProducts = $this->productCollectionFactory
            ->create()
            ->addIdFilter($parentIds)
            ->addAttributeToFilter('status', ['eq' => Status::STATUS_ENABLED])
            ->addAttributeToFilter('sku', ['nlike' => 'CUS%'])
            ->setVisibility([Visibility::VISIBILITY_BOTH, Visibility::VISIBILITY_IN_CATALOG, Visibility::VISIBILITY_IN_SEARCH]) //REVIEW AGAIN!
            ->addStoreFilter($this->storeManager->getStore($product->getStoreId()));
               $parentProducts->addAttributeToSelect('name');
               $parentProducts->addAttributeToSelect('visibility');
               $parentProducts->addAttributeToSelect('description');
               $parentProducts->addAttributeToSelect('image');
        if ($parentProducts->count() > 0) {
            return $parentProducts->getFirstItem();
        }

        return false;
    }

    /**
     * @param $productId
     * @return array|bool
     */
    private function getLinkedParent($productId)
    {
        $priceGrids = array($this->customLinks::LINK_TYPE_EXTRA_PRICEGRID_1,
            $this->customLinks::LINK_TYPE_EXTRA_PRICEGRID_2);

        $result = false;
        foreach ($priceGrids as $priceGrid) {
            $parentIds = $this->link->getParentIdsByChild($productId, $priceGrid);
            if (!empty($parentIds)) {
                $result = $this->getFirstParentVisibleAndEnabledAndNotCustom($parentIds);
            }

            if ($result) {
                return $result;
            }
        }
    }

    private function getFirstParentVisibleAndEnabledAndNotCustom(array $parentIds)
    {
        $collection = $this->productCollectionFactory->create();
        //$collection->addIdFilter($parentIds);
        $collection->addAttributeToFilter('visibility', ['neq' => Visibility::VISIBILITY_NOT_VISIBLE]);
        $collection->addAttributeToFilter('status', Status::STATUS_ENABLED);
        $collection->addAttributeToFilter('sku', ['nlike' => 'CUS%']);
        $collection->addAttributeToSelect('name');
        $collection->addAttributeToSelect('visibility');
        $collection->addAttributeToSelect('description');
        $collection->addAttributeToSelect('image');
        //$cond = $this->getConnection()->quoteInto('e.row_id in (?)', $parentIds);
        //$collection->getSelect()->joinInner($this->getConnection()->getTableName('catalog_product_entity'), $cond);
        $collection->addAttributeToFilter('row_id', ['in' => $parentIds]);

        if ($collection->count()) {
            return $collection->getFirstItem();
        }

        return false;
    }

    /**
     * @param $product
     * @return string[]
     */
    private function getConfigurableProductFromChildId($product)
    {
        $parentsByChild = $this->catalogProductTypeConfigurable->getParentIdsByChild($product->getId());
        return $parentsByChild;
    }

    /**
     * @param $product
     * @return array
     */
    private function getBundleProductFromChildId($product)
    {
        $productRowId = $this->getRowId($product->getId());
        return $this->bundleProduct->getParentIdsByChild($productRowId);
    }

    /**
     * @param $product
     * @param $parentsByChild
     * @return array
     */
    private function getGroupProductFromChildId($product)
    {
        $productRowId = $this->getRowId($product->getId());
        $parentsByChildRows = $this->groupedProduct->getParentIdsByChild($productRowId);
        //get entities
        $parentsByChild = [];
        foreach ($parentsByChildRows as $parentsByChildRow) {
            $parentsByChild[] = $this->getEntityId($parentsByChildRow);
        }
        return $parentsByChild;
    }

    private function getCacheKey($cacheKey)
    {
        if (isset($this->parentProducts[$cacheKey])) {
            return $this->parentProducts[$cacheKey];
        }

        return '';
    }

    private function cacheParentProduct($cacheKey, $value)
    {
        $this->parentProducts[$cacheKey] = $value;
    }


}