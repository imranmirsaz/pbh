<?php


namespace PBH\GoogleFeed\Model\ProductCollection\ConfigSetup;


use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Design\Theme\ThemeProviderInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\View\DesignInterface;
class ThemeSetup
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var ThemeProviderInterface
     */
    private $themeProvider;
    /**
     * @var DesignInterface
     */
    private $design;


    /**
     * ThemeSetup constructor.
     * @param StoreManagerInterface $storeManager
     * @param ScopeConfigInterface $scopeConfig
     * @param ThemeProviderInterface $themeProvider
     * @param DesignInterface $design
     */
    public function __construct(StoreManagerInterface $storeManager,
                                ScopeConfigInterface $scopeConfig,
                                ThemeProviderInterface $themeProvider,
                                DesignInterface $design)
    {
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->themeProvider = $themeProvider;
        $this->design = $design;
    }

    private function getThemeData()
    {
        $themeId = $this->scopeConfig->getValue(
            \Magento\Framework\View\DesignInterface::XML_PATH_THEME_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );

        /** @var $theme \Magento\Framework\View\Design\ThemeInterface */
      //  $theme = $this->storeManager->getThemeById($themeId);
        $theme = $this->themeProvider->getThemeById($themeId);


        return $theme->getData();
    }
    public function setDesignTheme(){
       $themeData = $this->getThemeData();
        $this->design->setDesignTheme($themeData['theme_path'],'frontend');


    }
}