<?php


namespace PBH\GoogleFeed\Model\ProductCollection;


use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ProductFactory;
use Magento\Store\Model\StoreManagerInterface;

class ProductReaderWithParentFallback
{
    /**
     * @var ParentProductFinder
     */
    private $parentProductFinder;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var ProductFactory
     */
    private $productFactory;


    private $productDataProvider;

    public function __construct(
        ParentProductFinder $parentProductFinder,
        StoreManagerInterface $storeManager,
        ProductFactory $productFactory,
        ProductDataProvider $productDataProvider

    )
    {
        $this->parentProductFinder = $parentProductFinder;
        $this->storeManager = $storeManager;
        $this->productFactory = $productFactory;
        $this->productDataProvider = $productDataProvider;
    }

    public function getProductVisibilityFallbackOnParentIfNotVisible($product)
    {
        $visibility = $product->getVisibility();

        if ($visibility == Visibility::VISIBILITY_NOT_VISIBLE) {

            $result = $this->parentProductFinder->getParentOrLinkedProduct($product);
            if (!empty($result)) {
                list($parentOrLinkedProduct, $productType) = $result;
                if ($parentOrLinkedProduct) {
                    $visibility = $parentOrLinkedProduct->getVisibility();
                }
            } else {
                $visibility = Visibility::VISIBILITY_NOT_VISIBLE;
            }
        }

        return $visibility;
    }

    public function getProductDescriptionFallbackOnParentIfNotVisible($product)
    {
        $description = $product->getDescription();

        if ($product->getVisibility() == Visibility::VISIBILITY_NOT_VISIBLE) {

            list($parentOrLinkedProduct, $productType) = $this->parentProductFinder->getParentOrLinkedProduct($product);
            if ($parentOrLinkedProduct) {
                if ($description == '') {
                    $description = $parentOrLinkedProduct->getDescription();
                }
            }
        }
        $shortDescription = $product->getShortDescription();
        return $this->cleanDescription($shortDescription . ' ' . $description);
    }


    public function getProductUrlFallbackOnParentIfNotVisible($product,$accessDataType = null)
    {
        $url = $this->addPostUrl($product->getProductUrl(), 'self');
        if ($product->getGoogleFeedUrl()) {

            $google_feed_url = $this->processGoogleFeedUrl($product->getGoogleFeedUrl());
            $url = $this->addPostUrl($google_feed_url, 'google_feed_url');
        }

        if ($product->getVisibility() == Visibility::VISIBILITY_NOT_VISIBLE) {

            list($parentOrLinkedProduct, $productType) = $this->parentProductFinder->getParentOrLinkedProduct($product);
            if ($parentOrLinkedProduct) {
                $url = $this->addPostUrl($parentOrLinkedProduct->getProductUrl(), $productType);
            }
        }

       return $this->cleanProductUrl($url,$accessDataType);
    }



    /**
     * @param $url
     * @param string $linktype
     * @return mixed|string
     */
    private function addPostUrl($url, $linktype = '')
    {
        $post_fix = '&utm_source=google&utm_medium=product_feed_or_listings';

        $result = $url;
        $result = str_replace("utm_source=google&utm_medium=product_feed_or_listings", "", $result);
        $result = rtrim($result, "?");
        $result = rtrim($result, "#");


        if (strpos($result, "?")) {
            $result = $result . "&utm_linktype=" . $linktype;
        } else {
            $result = $result . "?utm_linktype=" . $linktype;
        }
        $result = $result . $post_fix;

        return $result;
    }

    private function cleanDescription($description)
    {
        $description = str_replace(array("\r\n", "\n", "\r"), ' ', $description);
        $description = str_replace("</li>", ". ", $description);
        $description = strip_tags($description);
        $description = preg_replace('~<\s*\bscript\b[^>]*>(.*?)<\s*\/\s*script\s*>~is', '', $description);
        $description = str_replace("  ", " ", $description);
        $description = trim($description);

        return $description;
    }

    private function processGoogleFeedUrl($feedUrl)
    {
        $storeUrl = $this->storeManager->getStore()->getBaseUrl();
        $storeUrlParsed = parse_url($storeUrl);

        if (strpos($feedUrl, $storeUrl, 0) === true) {
            return $feedUrl;
        }

        $feedUrlParsed = parse_url($feedUrl);


        $feedUrlParsed['scheme'] = $storeUrlParsed['scheme'];
        $feedUrlParsed['host'] = $storeUrlParsed['host'];

        // a relative link many not have forward slash at the start.
        if (substr($feedUrlParsed['host'], -1) !== '/') {
            $feedUrlParsed['host'] = $feedUrlParsed['host'] . '/';
        }

        return $this->buildUrl($feedUrlParsed);
    }

    private function buildUrl(array $parts)
    {
        $scheme = isset($parts['scheme']) ? ($parts['scheme'] . '://') : '';
        $host = $parts['host'] ?? '';
        $port = isset($parts['port']) ? (':' . $parts['port']) : '';
        $user = $parts['user'] ?? '';
        $pass = isset($parts['pass']) ? (':' . $parts['pass']) : '';
        $pass = ($user || $pass) ? ($pass . '@') : '';
        $path = $parts['path'] ?? '';
        $query = isset($parts['query']) ? ('?' . $parts['query']) : '';
        $fragment = isset($parts['fragment']) ? ('#' . $parts['fragment']) : '';
        $url = implode('', [$user, $pass, $host, $port, $path, $query, $fragment]);
        $url = str_replace('//', '/', $url);

        return implode('', [$scheme, $url]);
    }

    public function getProductAttributeFallbackOnParentIfNotAvailable($product, $attribute_code)
    {

        $attribute_value = $product->getData($attribute_code);
        if ($attribute_value) return $attribute_value;

        return $this->getParentProductAttribute($product, $attribute_code);

    }


    public function getProductImageFallbackOnParentIfNotAvailable($product,$dataType = null)
    {
        if ($dataType == 'PBH_ElasticSearchIndexer') {
            return $this->getProductImageForIndexingFallbackOnParentIfNotAvailable($product);
        }

        return $this->getProductImageForGoogleFeedFallbackOnParentIfNotAvailable($product);
    }

    private function getProductImageForIndexingFallbackOnParentIfNotAvailable($product)
    {
        $productImage = $this->productDataProvider->getBaseImage($product);
        if ($this->productDataProvider->getPlaceholderImage() != $productImage) {
            return $productImage;
        }

        $parentProduct = $this->getParentProduct($product);
        if($parentProduct){
            return $this->productDataProvider->getBaseImage($parentProduct);
        }

    }

    private function getProductImageForGoogleFeedFallbackOnParentIfNotAvailable($product)
    {
        $attribute_value = $this->productDataProvider->getProductImage($product);

        if ($attribute_value) return $this->productDataProvider->attachImageBaseUrl($attribute_value);

        return $this->productDataProvider->attachImageBaseUrl($this->getParentProductAttribute($product, 'image'));
    }

    public function getProductCategoryNameFallbackOnParentIfNotAvailable($product)
    {
        $categoryIds = $this->getCategoryIdsFallbackToParent($product);
        if (empty($categoryIds)) return '';
        return $this->productDataProvider->getCategoriesNames($categoryIds);

    }


    private function getCategoryIdsFallbackToParent($product)
    {
        if (!empty($product->getCategoryIds())) {
            return $product->getCategoryIds();
        }

        return $this->getParentProductAttribute($product, 'category_ids');

    }


    private function getParentProduct($product)
    {
        $result = $this->parentProductFinder->getParentOrLinkedProduct($product);

        if (!empty($result)) {
            list($parentOrLinkedProduct, $productType) = $result;
            if ($parentOrLinkedProduct) {
               return $parentOrLinkedProduct;
            }
        }
    }

    public function getParentProductAttribute($product, string $attribute)
    {

        $parentProduct = $this->getParentProduct($product);


        if($parentProduct){
            if($attribute =='image'){
                return $parentProduct->getImage();
            }
            return $parentProduct->getData($attribute);
        }
    }

    private function cleanProductUrl(string $url, $accessDataType)
    {
        if($accessDataType =='PBH_ElasticSearchIndexer'){
          $url_parts = explode('?',$url);
          return $url_parts[0];
        }

        return $url;
    }


}