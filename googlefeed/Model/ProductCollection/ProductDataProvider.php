<?php


namespace PBH\GoogleFeed\Model\ProductCollection;


use PBH\GoogleFeed\Model\ProductCollection\ProductDataProvider\ImageDataProvider;
use PBH\GoogleFeed\Model\ProductCollection\ProductDataProvider\ProductCategoriesDataProvider;
use PBH\GoogleFeed\Model\ProductCollection\ProductDataProvider\ProductData;

class ProductDataProvider
{
    /**
     * @var ImageDataProvider
     */
    private $imageDataProvider;
    /**
     * @var ProductCategoriesDataProvider
     */
    private $productCategoriesDataProvider;
    /**
     * @var ProductData
     */
    private $productData;


    /**
     * ProductDataProvider constructor.
     * @param ImageDataProvider $imageDataProvider
     * @param ProductCategoriesDataProvider $productCategoriesDataProvider
     * @param ProductData $productData
     */
    public function __construct(ImageDataProvider $imageDataProvider,
                                ProductCategoriesDataProvider $productCategoriesDataProvider,
                                ProductData $productData)
    {
        $this->imageDataProvider = $imageDataProvider;
        $this->productCategoriesDataProvider = $productCategoriesDataProvider;
        $this->productData = $productData;
    }

    public function getCategoriesNames($categoryIds){

        if(empty($categoryIds)) return;
        return  $this->productCategoriesDataProvider->getCategoryNamesByIds($categoryIds);

    }

    public function attachImageBaseUrl($attribute_value)
    {
        return $this->imageDataProvider->attachImageBaseUrl($attribute_value);
    }

    public function getProductImage($product)
    {
        return $this->imageDataProvider->getProductImage($product);
    }

    public function getPlaceholderImage()
    {
        return $this->imageDataProvider->getPlaceholderImage();
    }

    public function getBaseImage($product)
    {
        return $this->imageDataProvider->getBaseImage($product);
    }
}