<?php


namespace PBH\GoogleFeed\Model\ProductCollection;

use Magento\Tax\Api\TaxCalculationInterface;

class ShippingReader
{
    CONST FREE_SHIPPING_THRASHHOLD_FOR_VATABLE_PRODUCTS = 59.99;
    CONST FREE_SHIPPING_THRASHHOLD_FOR_NONVATABLE_PRODUCTS = 49.99;
    CONST SHIPPING_COST = 3.54;
    /**
     * @var TaxCalculationInterface
     */
    private $taxCalculation;

    public function __construct(
        TaxCalculationInterface $taxCalculation
    ) {
        $this->taxCalculation = $taxCalculation;
    }

    public function getShippingPrice($product)
    {
        $code = $product->getTaxClassId();

        if ($code == 2) {
            $rate = $this->taxCalculation->getCalculatedRate($code);

            $priceIncludingTax = ( ($product->getPrice() * $rate) /  100 ) + $product->getPrice();
            if($priceIncludingTax > self::FREE_SHIPPING_THRASHHOLD_FOR_VATABLE_PRODUCTS){
                return 0;
            }
        }

        if ($code == 8){
            if($product->getPrice() > self::FREE_SHIPPING_THRASHHOLD_FOR_NONVATABLE_PRODUCTS){
                return 0;
            }
        }

        return self::SHIPPING_COST;
    }

    public function getProductPrice($product){

        if ($this->isProductVatable($product)) {

            $rate = $this->taxCalculation->getCalculatedRate($product->getTaxClassId());
            return ( ($product->getPrice() * $rate) /  100 ) + $product->getPrice();
        }

        return $product->getPrice();

    }


    private function isProductVatable($product){

        $code = $product->getTaxClassId();

        if($code == 2) {return true;}

        return false;
    }
}