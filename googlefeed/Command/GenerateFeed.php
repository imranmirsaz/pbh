<?php


namespace PBH\GoogleFeed\Command;


use Magento\Store\Model\StoreManagerInterface;
use PBH\ElasticSearchIndexer\Model\IndexProducts;
use PBH\GoogleFeed\Model\ProductsFeed;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class GenerateFeed extends Command
{

    private const STORE_ID = 'storeid';

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var ProductsFeed
     */
    private $productsFeed;


    /**
     * ProductReindex constructor.
     * @param ProductsFeed $productsFeed
     * @param StoreManagerInterface $storeManager
     * @param null $name
     */
    public function __construct(ProductsFeed $productsFeed, StoreManagerInterface $storeManager, $name = null)
    {
        $this->storeManager = $storeManager;
        $this->productsFeed = $productsFeed;
        parent::__construct($name);
    }


    protected function configure()
    {

        $storeIdParam = [
            new InputArgument(
                self::STORE_ID,
                InputArgument::REQUIRED,
                'The Store Id to index.'
            )
        ];

        $this->setName('feed:generate');
        $this->setDescription('Generate Google Feed File');
        $this->setDefinition($storeIdParam);

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $time = strtotime("NOW");
        $output->writeln("Google Feed Generation Started...");

        $storeId = $input->getArgument(self::STORE_ID);

        $this->productsFeed->generateFeed(null, $storeId);

        $secondsTaken = strtotime("NOW") - $time;
        $output->writeln(round(($secondsTaken / 60)) . " Minutes taken to generate google feed file.");

    }


}