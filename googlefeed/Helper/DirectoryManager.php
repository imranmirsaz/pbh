<?php


namespace PBH\GoogleFeed\Helper;


use Exception;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Io\File as IO;
use Magento\Framework\Module\Dir\Reader as ModuleReader;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

class DirectoryManager extends AbstractHelper
{
    const STORE_DIR_ACTIVE = true;
    const CUSTOM_DIR = 'feeds';
    const DS = '/';
    /**
     * @var DirectoryList
     */
    private $directoryList;
    /**
     * @var ModuleReader
     */
    private $moduleReader;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var IO
     */
    private $io;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    public function __construct(
        DirectoryList $directoryList,
        ModuleReader $moduleReader,
        LoggerInterface $logger,
        IO $io,
        StoreManagerInterface $storeManager,
        Context $context
    ) {
        parent::__construct($context);
        $this->directoryList = $directoryList;
        $this->moduleReader = $moduleReader;
        $this->logger = $logger;
        $this->io = $io;
        $this->storeManager = $storeManager;
    }

    public function getDirectoryPath(): string
    {
        $moduleRootDir = $this->getModuleRootDirectory();

        $customDirectory = $moduleRootDir . self::DS . self::CUSTOM_DIR;

        if ($this->io->fileExists($customDirectory) == false) {

            if ($this->makeDir($customDirectory)) {
                $customDirectory = $moduleRootDir . self::DS . self::CUSTOM_DIR;
            }
        }

        //check if store directory enabled
        if (self::STORE_DIR_ACTIVE == true) {
            $storeId = $this->storeManager->getStore()->getId();
            $customDirectory = $dirStorePath = $customDirectory . self::DS . $storeId;

            if ($this->io->fileExists($dirStorePath) == false) {
                if ($this->makeDir($dirStorePath)) {
                    $customDirectory = $dirStorePath;
                }
            }

        }

        return $customDirectory . self::DS;
    }

    public function getModuleRootDirectory()
    {
        $ModuleRootDir = $this->moduleReader->getModuleDir('', $this->_getModuleName());
        return $ModuleRootDir;
    }

    public function makeDir($dir): bool
    {
        try {
            if ($this->io->mkdir($dir, 0777)) {
                return true;
            }
        } catch (Exception $e) {
            $this->logger->critical('GoogleFeed:' . $e->getMessage());
        }
        return false;
    }

}