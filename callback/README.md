# magento-callback

Setup the module
Go to Content/Widget in Magento backend, add a widget for the home page.
Go to Store/Configuration/PBH/Callback and set the email where the request need to go
Flush the cache

Use the feature
Go to home page (if widget was added on home page)
hit the callback request image, once the form is successfully submitted, an email should land in the admin recipient with the details in the form
