<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace PBH\Callback\Controller\Index;

use Magento\Captcha\Helper\Data;
use Magento\Captcha\Observer\CaptchaStringResolver;
use Magento\Contact\Controller\Index;
use Magento\Contact\Model\ConfigInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use PBH\CallBack\Model\Mail;

class Ajax extends Index
{
    const FORM_ID = 'contact_us';
    /**
     * @var \PBH\CallBack\Model\Mail
     */
    private $mail;

    /**
     * @var Data
     */
    private $helper;
    /**
     * @var CaptchaStringResolver
     */
    private $captchaStringResolver;
    /**
     * @var JsonFactory
     */
    private $jsonFactory;

    /**
     * @param Context $context
     * @param ConfigInterface $contactsConfig
     * @param Data $data
     * @param Mail $mail
     * @param CaptchaStringResolver $captchaStringResolver
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        ConfigInterface $contactsConfig,
        Data $data,
        Mail $mail,
        CaptchaStringResolver $captchaStringResolver,
        JsonFactory $jsonFactory
    ) {
        parent::__construct($context, $contactsConfig);
        $this->mail = $mail;
        $this->helper = $data;
        $this->captchaStringResolver = $captchaStringResolver;
        $this->jsonFactory = $jsonFactory;
    }

    public function execute()
    {
        $result = $this->jsonFactory->create();
        $validatedRequest = $this->validatedParams();

        if (isset($validatedRequest['errors']) and $validatedRequest['errors'] == true) {
            $result->setData(['result' => $validatedRequest]);
            return $result;
        }

        $this->mail->send($validatedRequest);

        $message = __('Thanks for contacting us. We\'ll respond to you very soon.');
        $result->setData(['result' => ['errors' => false, 'message' => $message]]);

        return $result;
    }

    /**
     * @return array
     */
    private function validatedParams()
    {
        $request = $this->getRequest();
        if (!$this->validateCaptcha($request)) {
            return [
                'errors' => true,
                'message' => __('Incorrect CAPTCHA.')
            ];
        }

        if (trim($request->getParam('name')) === '') {
            return [
                'errors' => true,
                'message' => __('Name field is empty.')
            ];
        }

        if (trim($request->getParam('telephone')) === '') {
            return [
                'errors' => true,
                'message' => __('Telephone is missing.')
            ];
        }

        return $request->getParams();
    }

    /**
     * @param $request
     * @return bool
     */
    private function validateCaptcha($request)
    {
        $formId = self::FORM_ID;
        $captchaModel = $this->helper->getCaptcha($formId);
        if (!$captchaModel->isCorrect($this->captchaStringResolver->resolve($request, $formId))) {
            return false;
        }

        return true;
    }

}
