define(
    [
        'jquery',
        'Magento_Ui/js/modal/modal'
    ],
    function($, modal) {

        function activateButton(button)
        {
            button.attr("disabled", true);
        }

        function disactivateButton(button)
        {
            button.attr("disabled", false);
        }

        function resetForm(form) {
            form.trigger("reset");
        }

        return function (config, element) {
            var options = {
                type: 'popup',
                responsive: true,
                innerScroll: true,
                title: 'Request a Callback',
                buttons: []
            };
            var openModal = modal(options, $('#popup-modal'));

            var form = $('#contact_us');
            var submitButton = form.find('.action.submit');
            var openFormLink = $('.open-contact-form');

            openFormLink.click(function () {
                $('.callback-form-popup').show();
                $('.callback-success-block').hide();
                disactivateButton(submitButton);
                resetForm(form);
                $('.captcha-reload').click();
                $('#popup-modal').modal('openModal');
            });

            submitButton.click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();

                if (form.validation('isValid')) {
                    activateButton(submitButton);
                    $('#notification-message').html("");
                    $.ajax({
                        type: 'post',
                        url: config.form_action,
                        data: form.serialize(),
                        cache: false,
                        //    showLoader: 'true',
                        success: function (response) {
                            $('#captcha_contact_us').val("");
                            $('.captcha-reload').click();
                            if (response['result'].errors === false) {
                                $('.callback-form-popup').hide();
                                $('.callback-success-block').show();
                            } else {
                                disactivateButton(submitButton);
                            }

                            $('#notification-message').html(response['result'].message);
                        },
                        error: function () {
                            $("#notification-message").html("Error in processing the form");
                        }
                    });

                }

                return false;
            });
        }
    }
);