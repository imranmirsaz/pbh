<?php

namespace PBH\CallBack\Model;

use Magento\Store\Model\ScopeInterface;

class Mail
{
    const FALLBACK_EMAIL_PATH = 'trans_email/ident_sales/email';
    /**
     * @var \Magento\Framework\Escaper
     */
    private $_escaper;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    private $inlineTranslation;
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    private $_transportBuilder;

    /**
     * Mail constructor.
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Escaper $escaper
     */
    public function __construct(
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Escaper $escaper
    ) {
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->_escaper = $escaper;
    }

    public function send($post)
    {
        $this->inlineTranslation->suspend();

        try {
            $postObject = new \Magento\Framework\DataObject();
            $postObject->setData($post);
            $to  = $this->getConfigValue('pbh_callback/email_addresses/callback');
            $sender = [
                'name' =>  $this->getConfigValue('trans_email/ident_general/name'),
                'email' =>  $this->getConfigValue('trans_email/ident_general/email'),
            ];

            $templateVars = [
                'note' => $post['note'],
                'name' => $post['name'],
                'telephone' => $post['telephone']
            ];

            $transport = $this->_transportBuilder
                ->setTemplateIdentifier('callback_template') // this code we have mentioned in the email_templates.xml
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND, // this is using frontend area to get the template file
                        'store' => $this->storeManager->getStore()->getId(),
                    ]
                )
                ->setTemplateVars($templateVars)
                ->setFrom($sender)
                ->addTo($to)
                ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->inlineTranslation->resume();
        }
    }

    private function getConfigValue($path)
    {
        $pbhEmail = $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE);
        if ($pbhEmail) {
            return $pbhEmail;
        }

        return $this->scopeConfig->getValue(self::FALLBACK_EMAIL_PATH, ScopeInterface::SCOPE_STORE);
    }
}