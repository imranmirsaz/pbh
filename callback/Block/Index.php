<?php
namespace PBH\CallBack\Block;

use Magento\Framework\View\Element\Template;

class Index extends Template
{
    public function getCallBackTime(){
        return $times = [
            ['value'=>'Callback: ASAP','label'=>'ASAP'],
            ['value'=>'Callback: In 1 hour','label'=>'In 1 hour'],
            ['value'=>'Callback: This week','label'=>'This week'],
            ['value'=>'Callback: Next week','label'=>'Next week']
        ];
    }

    public function getFormAction()
    {
        return $this->getUrl('callback/index/post', ['_secure' => true]);
    }

    public function getFormAjaxAction()
    {
        return $this->getUrl('callback/index/ajax', ['_secure' => true]);
    }
}
