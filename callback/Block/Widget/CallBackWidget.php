<?php
namespace PBH\CallBack\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Widget\Block\BlockInterface;
use PBH\CallBack\Block\Index;

class CallBackWidget extends Template implements BlockInterface
{
    /**
     * @var Index
     */
    private $index;

    public function __construct(
        Context $context,
        Index $index,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->setTemplate('Widget/form_link.phtml');
        $this->index = $index;
    }

    public function getCallBackTime()
    {
        return $this->index->getCallBackTime();
    }

    public function getFormAction()
    {
        return $this->index->getFormAction();
    }

}