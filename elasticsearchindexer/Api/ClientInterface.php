<?php


namespace PBH\ElasticSearchIndexer\Api;


/**
 * Interface ClientInterface
 * @package PBH\ElasticSearchIndexer\Api
 */
Interface ClientInterface
{
    /**
     * @return mixed
     */
    public function getConfig();
}