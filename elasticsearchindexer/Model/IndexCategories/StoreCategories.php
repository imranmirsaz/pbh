<?php


namespace PBH\ElasticSearchIndexer\Model\IndexCategories;


use Magento\Catalog\Helper\Category;

class StoreCategories
{
    /**
     * @var Category
     */
    private $category;

    /**
     * StoreCategories constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * @param $storeId
     * @return array
     */
    public function getStoreCategoryIds($storeId):array
    {
        $categories = $this->category->getStoreCategories(true, true);
        $categoryIds = array();
        foreach ($categories as $category) {
            $categoryIds[] = $category->getId();
        }
        return $categoryIds;
    }
}