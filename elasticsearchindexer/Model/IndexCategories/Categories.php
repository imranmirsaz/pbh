<?php

namespace PBH\ElasticSearchIndexer\Model\IndexCategories;

use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;


/**
 * Class Categories
 * @package PBH\ElasticSearchIndexer\Model\IndexCategories
 */
class Categories
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var StoreCategories
     */
    private $storeCategories;

    /**
     * StoreDataManager constructor.
     * @param CollectionFactory $collectionFactory
     * @param StoreCategories $storeCategories
     */
    public function __construct(CollectionFactory $collectionFactory, StoreCategories $storeCategories)
    {
        $this->storeCategories = $storeCategories;
        $this->collectionFactory = $collectionFactory;
    }

    public function getStoreCategories($storeId)
    {
        $categoriesIds = $this->storeCategories->getStoreCategoryIds($storeId);
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('entity_id', array('in' => $categoriesIds));
        $collection->addAttributeToSelect('*');
        return $collection;
    }


}






