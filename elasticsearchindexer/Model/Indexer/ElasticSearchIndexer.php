<?php


namespace PBH\ElasticSearchIndexer\Model\Indexer;


use PBH\ElasticSearchIndexer\Model\ElasticSearchClient;

/**
 * Class ElasticSearchIndexer
 * @package PBH\ElasticSearchIndexer\Model\Indexer
 */
abstract class ElasticSearchIndexer
{
    /**
     * @var ElasticSearchClient
     */
    private $elasticSearchClient;


    /**
     * ElasticSearchIndexer constructor.
     * @param ElasticSearchClient $elasticSearchClient
     */
    public function __construct(ElasticSearchClient $elasticSearchClient)
    {
        $this->elasticSearchClient = $elasticSearchClient;
    }

    /**
     * @param $storeId
     */
    public function run($storeId)
    {

        if (!$this->elasticSearchClient->checkConnection($storeId)) return;
        $prefix = $this->elasticSearchClient->getIndexPrefix();
        $indexName = $this->getIndexName($prefix.$storeId);
        $elasticSearchRecords = $this->getDataToBeIndexed($storeId,$indexName);
        $this->elasticSearchClient->resetIndex($indexName);
        $this->elasticSearchClient->setElasticSearchDocumentBulk(array('body' => $elasticSearchRecords));
        $this->elasticSearchClient->refreshIndex($indexName);
        return $this->getIndexedDocumentsCount();
    }

    /**
     * @param $storeId
     * @param $indexName
     * @return mixed
     */
    abstract function getDataToBeIndexed($storeId,$indexName);

    /**
     * @param $prefixAndStoreId
     * @return mixed
     */
    abstract function getIndexName($prefixAndStoreId);

    /**
     * @return mixed
     */
    abstract function getIndexedDocumentsCount();
}