<?php


namespace PBH\ElasticSearchIndexer\Model;


class IndexCatalog
{
    /**
     * @var IndexCategories
     */
    private $indexCategories;
    /**
     * @var IndexProducts
     */
    private $indexProducts;


    /**
     * IndexCatalog constructor.
     * @param IndexCategories $indexCategories
     * @param IndexProducts $indexProducts
     */
    public function __construct(IndexCategories $indexCategories, IndexProducts $indexProducts)
    {
        $this->indexCategories = $indexCategories;
        $this->indexProducts = $indexProducts;
    }
    public function indexCategories($storeId){

        return $this->indexCategories->run($storeId);

    }

    public function indexProducts($storeId){

        return  $this->indexProducts->run($storeId);

    }


}