<?php


namespace PBH\ElasticSearchIndexer\Model;


use PBH\ElasticSearchIndexer\Model\Indexer\ElasticSearchIndexer;
use PBH\GoogleFeed\Model\ProductsCollection;


class IndexProducts extends ElasticSearchIndexer
{
    static protected $indexedDocumentsCount = 0;
    private const INDEX_POSTFIX = '_product_index';

    /**
     * @var ElasticSearchClient
     */
    private $elasticSearchClient;
    /**
     * @var ProductsCollection
     */
    private $productsCollection;

    /**
     * ProductsIndexer constructor.
     * @param ProductsCollection $productsCollection
     * @param ElasticSearchClient $elasticSearchClient
     */
    public function __construct(ProductsCollection $productsCollection, ElasticSearchClient $elasticSearchClient)
    {
        parent::__construct($elasticSearchClient);
        $this->productsCollection = $productsCollection;
    }


    /**
     * @param $storeId
     * @param $selectedIndexName
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    function getDataToBeIndexed($storeId,$selectedIndexName)
    {
        $elasticSearchRecords = array();
        $productToPush = array();
        $storeProducts = $this->productsCollection->getProducts(null, $storeId,'PBH_ElasticSearchIndexer');

        foreach ($storeProducts as $storeProduct) {

            $product = $this->productsCollection->processProduct($storeProduct);
            if(!$product) continue;
            $productToPush['link'] = $product['link'];
            $productToPush['image_link'] = $product['image_link'];
            $productToPush['id'] = $product['id'];
            $productToPush['name'] = $product['title'];
            $productToPush['description'] = $product['description'];
            $productToPush['color'] = $product['color'];
            if($productToPush['color'] === false){
                $productToPush['color'] = '';
            }
            $productToPush['size'] = $product['size'];
            $productToPush['material'] = $product['material'];
            $productToPush['sales_code'] = $product['sales_code'];
            $productToPush['barcode'] = $product['gtin'];
            $productToPush['meta_title'] = $product['meta_title'];
            $productToPush['meta_keywords'] = $product['meta_keywords'];

            $productToPush['product_flash_id'] = $product['product_flash_id'];
            $productToPush['no_of_reviews'] = $product['no_of_reviews'];
            $productToPush['ratings'] = $product['ratings'];
            $productToPush['category_name'] = $product['category_name'];
            $productToPush['parent_name'] = $product['parent_name'];

            $elasticSearchRecords[] =
                array('index' =>
                    array(
                        '_index' => $selectedIndexName,
                        '_id' => $productToPush['id'],
                        '_type' => 'product'
                    )
                );

            $elasticSearchRecords[] =['body' => $productToPush];
            self::$indexedDocumentsCount += 1;

        }
        return   $elasticSearchRecords;
    }

    /**
     * @param $storeId
     * @return mixed
     */
    function getIndexName($storeId)
    {
        return $storeId . self::INDEX_POSTFIX;
    }

    /**
     * @return mixed
     */
    function getIndexedDocumentsCount()
    {
        return self::$indexedDocumentsCount;
    }
}