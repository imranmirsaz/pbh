<?php


namespace PBH\ElasticSearchIndexer\Model;

use Elasticsearch\ClientBuilder;
use Magento\Search\Model\ResourceModel\SynonymGroup\CollectionFactory as SynonymsCollection;
use PBH\ElasticSearchIndexer\Model\ElasticSearchClient\ElasticSearchStoreConfig;
use Psr\Log\LoggerInterface;

/**
 * Class ElasticSearchClient
 * @package PBH\ElasticSearchIndexer\Model
 */
class ElasticSearchClient
{
    static private $logEnabled = true;
    private $analyzersAndFilters;
    /**
     * @var ClientBuilder
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var ClientBuilder
     */
    private $clientBuilder;
    /**
     * @var ElasticSearchStoreConfig
     */
    private $elasticSearchStoreConfig;
    /**
     * @var SynonymsCollection|CollectionFactory
     */
    private $synonymsCollection;

    /**
     * IndexManager constructor.
     * @param ClientBuilder $clientBuilder
     * @param LoggerInterface $logger
     * @param ElasticSearchStoreConfig $elasticSearchStoreConfig
     * @param CollectionFactory $synonymsCollection
     */
    public function __construct(ClientBuilder $clientBuilder, LoggerInterface $logger,
                                ElasticSearchStoreConfig $elasticSearchStoreConfig,
                                SynonymsCollection $synonymsCollection )
    {
        $this->clientBuilder = $clientBuilder;
        $this->logger = $logger;
        $this->elasticSearchStoreConfig = $elasticSearchStoreConfig;
        $this->buildClient();
        $this->synonymsCollection = $synonymsCollection;
    }

    private function buildClient()
    {

        $hosts = [
            $this->getConnectionUrl()
        ];


        $this->client = $this->clientBuilder::create()->setHosts($hosts)->build();

    }

    private function getConnectionUrl()
    {

      $elasticSearchHostDetails = $this->elasticSearchStoreConfig->getEndPoint();


        return $elasticSearchHostDetails;

    }

    public function checkConnection()
    {

        return $this->client->cluster()->stats();
    }

    public function setElasticSearchDocumentBulk($elasticSearchRecords)
    {
        $results = $this->client->bulk($elasticSearchRecords);
    }

    public function resetIndex(string $selectedIndexName)
    {

        $this->deleteOldElasticSearchIndex($selectedIndexName);
        $this->createIndex($selectedIndexName);

    }

    private function deleteOldElasticSearchIndex($indexName)
    {


        if (!$this->indexExists($indexName)) return false;
        $deleteParams = [
            'index' => $indexName
        ];

        return $this->client->indices()->delete($deleteParams);


    }

    private function indexExists($indexName)
    {

        $indexParams['index'] = $indexName;
        return $this->client->indices()->exists($indexParams);
    }

    private function createIndex(string $selectedIndexName)
    {
        $params = [
            'index' => $selectedIndexName,
            'body' => [
                'settings' => [
                    'number_of_shards' => 2,
                    'number_of_replicas' => 0,
                    'index' => [
                        "analysis" => $this->getAnalyzersAndFilters()
//                            "analyzer" => [
//                                "default" => [
//                                    "tokenizer" => "standard",
//                                    "filter" => ["lowercase", "my_ascii_folding"]
//                                ],
//                                "synonym" => [
//                                    "tokenizer" => "whitespace",
//                                    "filter" => ["synonym"]
//                                ]
//                            ],
//                            "filter" => [
//                                "synonym" => [
//                                    "type" => "synonym",
//                                    "synonyms" => [$this->getSynonymsData()]
//                                ],
//                                "my_ascii_folding" => [
//                                    "type" => "asciifolding",
//                                    "preserve_original" => true
//                                ]
//                            ]


                    ]
                ]


            ]
        ];

        return $this->client->indices()->create($params);

    }

    public function refreshIndex($indexName)
    {
        $this->client->indices()->refresh(array('index' => $indexName));
    }

    private function addLog($message): void
    {
        if (self::$logEnabled) {
            $this->logger->info($message);
        }
    }

    public function getIndexPrefix(){
      return  $this->elasticSearchStoreConfig->getIndexPrefix();
    }

    private function getSynonymsData(){


        $synonyms = $this->synonymsCollection->create();
      $synonymsData = '';

      foreach ($synonyms as $synonym){
            $synonymsData .= '"'. $synonym->getSynonyms().'"' . ",\r\n";
      }

    return $synonymsData;
    }

    private function getAnalyzersAndFilters()
    {
        $this->getAssciAnalyzerAndFilter();
        $this->getSynonymsAnalyzerAndFilter();

        return $this->analyzersAndFilters;


    }

    private function getAssciAnalyzerAndFilter()
    {
        $indexAsciFoldingCheck = $this->elasticSearchStoreConfig->IndexAssciFoldingCheck();
        if($indexAsciFoldingCheck == false) return '';

        $this->analyzersAndFilters['analyzer']["default"] =  [
            "tokenizer" => "standard",
            "filter" => ["lowercase", "my_ascii_folding"]
        ];
        $this->analyzersAndFilters['filter']["my_ascii_folding"] = [
            "type" => "asciifolding",
            "preserve_original" => true
        ];

    }

    private function getSynonymsAnalyzerAndFilter()
    {

      $indexMagentoSynonymsCheck = $this->elasticSearchStoreConfig->IndexMagentoSynonymsCheck();
      if($indexMagentoSynonymsCheck == false) return '';

        $this->analyzersAndFilters['analyzer']["synonym"] = [
            "tokenizer" => "whitespace",
            "filter" => ["synonym"]
        ];
        $this->analyzersAndFilters['filter']["synonym"] =  [
            "type" => "synonym",
            "synonyms" => [$this->getSynonymsData()]
        ];

    }

}