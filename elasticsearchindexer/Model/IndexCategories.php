<?php


namespace PBH\ElasticSearchIndexer\Model;


use PBH\ElasticSearchIndexer\Model\IndexCategories\Categories;
use PBH\ElasticSearchIndexer\Model\Indexer\ElasticSearchIndexer;

/**
 * Class IndexCategories
 * @package PBH\ElasticSearchIndexer\Model
 */
class IndexCategories extends ElasticSearchIndexer
{
    private const INDEX_POSTFIX = '_category_index';
    static protected $indexedDocumentsCount = 0;
    /**
     * @var ElasticSearchClient
     */
    private $elasticSearchClient;
    /**
     * @var Categories
     */
    private $categories;

    /**
     * CategoriesIndexer constructor.
     * @param Categories $categories
     * @param ElasticSearchClient $elasticSearchClient
     */
    public function __construct(Categories $categories,ElasticSearchClient $elasticSearchClient)
    {
        $this->categories = $categories;
        parent::__construct($elasticSearchClient);
    }


    public function getDataToBeIndexed($storeId,$selectedIndexName){

        $storeCategories = $this->categories->getStoreCategories($storeId);
        if (count($storeCategories) < 1) return;

        $categoryToPush = array();


        $elasticSearchRecords = array();
        foreach ($storeCategories as $storeCategory) {

            $categoryToPush['link'] = $storeCategory->getUrl();
            $categoryToPush['image_url'] = $storeCategory->getImage();
            $categoryToPush['id'] = $storeCategory->getId();
            $categoryToPush['name'] = $storeCategory->getName();
            $categoryToPush['description'] = $this->cleanDescription($storeCategory->getShortDescription() . ' ' . $storeCategory->getDescription());
            $categoryToPush['meta_title'] = $storeCategory->getMetaTitle();
            $categoryToPush['meta_keywords'] = $storeCategory->getMetaKeywords();

            $elasticSearchRecords[] =
                ['index' =>
                    [
                        '_index' => $selectedIndexName,
                        '_id' => $categoryToPush['id'],
                        '_type' => 'category'
                    ]
                ];
            $elasticSearchRecords[] = $categoryToPush;
            self::$indexedDocumentsCount += 1;

        }
        return   $elasticSearchRecords;
    }

    public function getIndexName($storeId){
        return $storeId . self::INDEX_POSTFIX;
    }

    private function cleanDescription($description)
    {
        $description = str_replace(array("\r\n", "\n", "\r"), ' ', $description);
        $description = str_replace("</li>", ". ", $description);
        $description = strip_tags($description);
        $description = preg_replace('~<\s*\bscript\b[^>]*>(.*?)<\s*\/\s*script\s*>~is', '', $description);
        $description = str_replace("  ", " ", $description);
        $description = trim($description);

        return $description;
    }

    public function getIndexedDocumentsCount()
    {
      return self::$indexedDocumentsCount;
    }
}