<?php


namespace PBH\ElasticSearchIndexer\Model\ElasticSearchClient;


use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use PBH\ElasticSearchIndexer\Api\ClientInterface;

class ElasticSearchStoreConfig implements ClientInterface
{

    private const CATEGORY_INDEX_POSTFIX = '_category_index';
    private const PRODUCT_INDEX_POSTFIX = '_product_index';
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * StoreConfig constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(ScopeConfigInterface $scopeConfig,StoreManagerInterface $storeManager)
    {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    public function getEndPoint(){


        $urlComponents =  $this->getEndPointConnectionLink();

        $elasticSearchHostDetails = $urlComponents['scheme']
            . '://'
            . $urlComponents['user']
            . ':'
            . $urlComponents['pass']
            . '@'
            . $urlComponents['host']
            . ':'
            . $urlComponents['port'];
        return $elasticSearchHostDetails;
        //return 'http://51.75.121.7';

    }

    public function getConfig()
    {
        $hostName = array('hostname'=>$this->getEndPointConnectionLink());
        $categoryIndex =  array('category_index'=>$this->getIndex('category'));
        $productIndex =  array('product_index'=>$this->getIndex('product'));
        return [$hostName, $categoryIndex, $productIndex];
    }

    public function getIndexPrefix()
    {
        $indexPrefix =   $this->scopeConfig->getValue('catalog/search/elasticsearch_index_prefix');
        if($indexPrefix){
            return  $indexPrefix;
        }
        return '';
    }


    private function getIndex($indexType)
    {
        $indexPostFix = self::CATEGORY_INDEX_POSTFIX;
        if($indexType == 'product'){
            $indexPostFix = self::PRODUCT_INDEX_POSTFIX;
        }

        $prefix = $this->getIndexPrefix();
        $storeId = $this->storeManager->getStore()->getId();
        return $prefix.$storeId.$indexPostFix;
    }

    private function getEndPointConnectionLink()
    {
        $url = $this->scopeConfig->getValue('catalog/search/elasticsearch_server_hostname');
        $urlSections = explode('://',$url);
        $hostname = end( $urlSections);
        return [
            'host' => $hostname,
            'port' => $this->scopeConfig->getValue('catalog/search/elasticsearch_server_port'),
            'scheme' => parse_url($url, PHP_URL_SCHEME),
            'user' => $this->scopeConfig->getValue('catalog/search/elasticsearch_username'),
            'pass' => $this->scopeConfig->getValue('catalog/search/elasticsearch_password')
        ];

    }

    public function IndexMagentoSynonymsCheck()
    {
        return  $this->scopeConfig->getValue('pbh_elasticsearchIndexer/index_attributes/index_magento_synonyms',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

    }

    public function IndexAssciFoldingCheck()
    {
        return  $this->scopeConfig->getValue('pbh_elasticsearchIndexer/index_attributes/index_asci_folding',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

    }


}