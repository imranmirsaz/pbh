<?php


namespace PBH\ElasticSearchIndexer\Command;


use Magento\Store\Model\StoreManagerInterface;
use PBH\ElasticSearchIndexer\Model\IndexProducts;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class ProductReindex extends Command
{

    private const STORE_ID = 'storeid';

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var IndexProducts
     */
    private $indexProducts;

    /**
     * ProductReindex constructor.
     * @param IndexProducts $indexProducts
     * @param StoreManagerInterface $storeManager
     * @param null $name
     */
    public function __construct(IndexProducts $indexProducts, StoreManagerInterface $storeManager, $name = null)
    {
        $this->indexProducts = $indexProducts;
        $this->storeManager = $storeManager;
        parent::__construct($name);

    }


    protected function configure()
    {

        $storeIdParam = [
            new InputArgument(
                self::STORE_ID,
                InputArgument::REQUIRED,
                'The Store Id to index.'
            )
        ];

        $this->setName('index:productreindex');
        $this->setDescription('Reindex Products to ElasticSearch Instance');
        $this->setDefinition($storeIdParam);

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Products Reindexing Started...");

        $storeId = $input->getArgument(self::STORE_ID);

        $indexedDocuments = $this->indexProducts->run($storeId);

        $output->writeln($indexedDocuments . " Products Processed.");
    }


}