<?php


namespace PBH\ElasticSearchIndexer\Command;


use Magento\Store\Model\StoreManagerInterface;
use PBH\ElasticSearchIndexer\Model\IndexCategories;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class CategoryReindex extends Command
{

    private const STORE_ID = 'storeid';

    /**
     * @var IndexCategories
     */
    private $indexCategories;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * CategoryReindex constructor.
     * @param IndexCategories $indexCategories
     * @param StoreManagerInterface $storeManager
     * @param null $name
     */
    public function __construct(IndexCategories $indexCategories, StoreManagerInterface $storeManager, $name = null)
    {
        $this->indexCategories = $indexCategories;
        $this->storeManager = $storeManager;
        parent::__construct($name);

    }


    protected function configure()
    {

        $storeIdParam = [
            new InputArgument(
                self::STORE_ID,
                InputArgument::REQUIRED,
                'The Store Id to index.'
            )
        ];

        $this->setName('index:categoryreindex');
        $this->setDescription('Reindex Categories to ElasticSearch Instance');
        $this->setDefinition($storeIdParam);

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {


        $output->writeln("Categories Reindexing Started...");

        $storeId = $input->getArgument(self::STORE_ID);

        $this->storeManager->setCurrentStore($storeId);
        $this->storeManager->getStore()->getId(); // throws an exception if store doesn't exist

        $indexedDocuments = $this->indexCategories->run($storeId);

        $output->writeln($indexedDocuments . " Categories Processed.");
    }


}