<?php


namespace PBH\ElasticSearchIndexer\Command;


use Magento\Store\Model\StoreManagerInterface;
use PBH\ElasticSearchIndexer\Model\IndexCatalog;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class CatalogReindex extends Command
{

    private const STORE_ID = 'storeid';

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var IndexCatalog
     */
    private $indexCatalog;

    /**
     * ProductReindex constructor.
     * @param IndexCatalog $indexCatalog
     * @param StoreManagerInterface $storeManager
     * @param null $name
     */
    public function __construct(IndexCatalog $indexCatalog, StoreManagerInterface $storeManager, $name = null)
    {
        $this->storeManager = $storeManager;
        $this->indexCatalog = $indexCatalog;
        parent::__construct($name);

    }


    protected function configure()
    {

        $storeIdParam = [
            new InputArgument(
                self::STORE_ID,
                InputArgument::REQUIRED,
                'The Store Id to index.'
            )
        ];

        $this->setName('index:reindexall');
        $this->setDescription('Reindex Catalog to ElasticSearch Instance');
        $this->setDefinition($storeIdParam);

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Catalog Reindexing Started...");



        $storeId = $input->getArgument(self::STORE_ID);

        $this->storeManager->setCurrentStore($storeId);
        $this->storeManager->getStore()->getId(); // throws an exception if store doesn't exist

        $output->writeln("Processing Categories...");
        $indexedDocuments = $this->indexCatalog->indexCategories($storeId);
        $output->writeln($indexedDocuments . " Categories Processed.");

        $indexedDocuments = $this->indexCatalog->indexProducts($storeId);
        $output->writeln($indexedDocuments . " Products Processed.");
    }


}