# magento-contactus

## Install the Module
to enable the module, 
```cd app/code/PBH/
git clone git@github.com:poolebayholdingsltd/magento-contactus.git ContactUs
cd <Magento_Root>
php bin/magento setup:upgrade
```

## Check the module works
```go to <magento_url>/contact ```
and check the **PBH contact us** page loads

## Revert to Magento Contact feature:
To revert to Magento Contact Feature, disable the module
``` php bin/magento module:disable PBH_ContactUs
 go to <magento_url>/contact 
 ```
 and check the **Magento contact us** page loads

## Set Receiver Emails in Config
After Installing the module, please setup recipient's emails in admin.
To do so go to Admin Config -> Stores -> Configurations  -> PBH -> ContactUs 