<?php


namespace PBH\ContactUs\Model;


use Magento\Framework\App\Config\ScopeConfigInterface;

class ConfigEmails
{
    const FALLBACK_EMAIL_PATH = 'trans_email/ident_sales/email';
    private static $GENERAL_ENQUIRY = 1;

    private static $MARKETING_ENQUIRY = 2;

    private static $PAYMENT_ENQUIRY = 3;

    private static $TECHNICAL_ENQUIRY = 4;

    private static $SAMPLE_ENQUIRY = 5;

    private static $OTHER_ENQUIRY = 6;

    private static $QUOTATION_ENQUIRY = 7;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    public function getConfigValue($path)
    {
        $pbhEmail = $this->scopeConfig->getValue($path,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if($pbhEmail){
           return $pbhEmail;
        }
        return $this->scopeConfig->getValue(self::FALLBACK_EMAIL_PATH,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getConfigEmail(int $mailValueId)
    {
        if ($mailValueId == self::$GENERAL_ENQUIRY
            or $mailValueId == self::$MARKETING_ENQUIRY
            or $mailValueId == self::$SAMPLE_ENQUIRY
            or $mailValueId == self::$OTHER_ENQUIRY) {
            return $this->getConfigValue('pbh_contactus/email_addresses/sales');
        }

        if ($mailValueId == self::$PAYMENT_ENQUIRY) {
            return $this->getConfigValue('pbh_contactus/email_addresses/finance');
        }

        if ($mailValueId == self::$TECHNICAL_ENQUIRY) {
            return $this->getConfigValue('pbh_contactus/email_addresses/web_support');
        }

        return $this->getConfigValue('pbh_contactus/email_addresses/quotation');
    }

    /**
     * @return array
     */
    public function getEnquiryTypeOptions()
    {
        $options[self::$GENERAL_ENQUIRY] = 'Customer support and sales enquiries';
        $options[self::$MARKETING_ENQUIRY] = 'Product suggestions and marketing';
        $options[self::$PAYMENT_ENQUIRY] = 'Billing and payment enquiries';
        $options[self::$TECHNICAL_ENQUIRY] = 'Website technical problems';
        $options[self::$SAMPLE_ENQUIRY] = 'Free samples';
        $options[self::$OTHER_ENQUIRY] = 'Other';
        $options[self::$QUOTATION_ENQUIRY] = 'Quotations';

        return $options;
    }
}