<?php
/**
 * Created by PhpStorm.
 * User: herve.tribouilloy
 * Date: 21/11/19
 * Time: 10:40
 */

namespace PBH\ContactUs\Plugin;


class ContactForm
{
    /**
     * @var \PBH\ContactUs\ViewModel\FormModel
     */
    private $formModel;

    public function __construct(
        \PBH\ContactUs\ViewModel\FormModel $formModel
    ) {
        $this->formModel = $formModel;
    }

    public function afterGetTemplate(\Magento\Contact\Block\ContactForm $subject)
    {
        return 'PBH_ContactUs::form.phtml';
    }

    public function aroundGetData(\Magento\Contact\Block\ContactForm $subject, callable $proceed, $key)
    {
        if ($key == 'viewModel') {
            $result = $this->formModel;
        } else {
            $result = $proceed($key);
        }

        return $result;
    }
}