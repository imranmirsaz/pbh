<?php
/**
 * Created by PhpStorm.
 * User: herve.tribouilloy
 * Date: 21/11/19
 * Time: 09:32
 */

namespace PBH\ContactUs\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

class FormModel implements ArgumentInterface
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $urlBuilder;
    /**
     * @var \PBH\ContactUs\Model\ConfigEmails
     */
    private $configEmails;

    public function __construct(
        \Magento\Framework\UrlInterface $urlBuilder,
        \PBH\ContactUs\Model\ConfigEmails $configEmails
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->configEmails = $configEmails;
    }

    /**
     * Returns action url for contact form
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('contact/index/post', ['_secure' => true]);
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->urlBuilder->getUrl($route, $params);
    }

    /**
     * @return array
     */
    public function getEnquiryTypesOptions()
    {
        return $this->configEmails->getEnquiryTypeOptions();
    }
}
